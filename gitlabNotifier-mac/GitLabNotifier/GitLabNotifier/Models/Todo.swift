//
//  Todo.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 22/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

final class Todo: Decodable {

    enum TargetType: String, Decodable {

        case issue = "Issue"
        case mergeRequest = "MergeRequest"
    }

    enum Action: String, Decodable {

        case assigned
        case mentioned
        case buildFailed = "build_failed"
        case marked
        case approvalRequired = "approval_required"
        case unmergeable
        case directlyAddressed = "directly_addressed"
    }

    enum State: String, Decodable {

        case done
        case pending
    }

    let id: Int
    let project: Project
    let author: User
    let actionName: Action
    let targetType: TargetType
    let target: Target
    let targetUrl: String
    let body: String
    let state: State
    let createdAt: Date

    enum CodingKeys: String, CodingKey {

        case id
        case project
        case author
        case actionName = "action_name"
        case targetType = "target_type"
        case target
        case targetUrl = "target_url"
        case body
        case state
        case createdAt = "created_at"
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(Int.self, forKey: .id)
        self.project = try container.decode(Project.self, forKey: .project)
        self.author = try container.decode(User.self, forKey: .author)
        self.actionName = try container.decode(Action.self, forKey: .actionName)
        self.targetType = try container.decode(TargetType.self, forKey: .targetType)

        switch targetType {

        case .mergeRequest:
            let mergeRequest = try container.decode(MergeRequest.self, forKey: .target)
            self.target = mergeRequest

        case .issue:
            let issue = try container.decode(Issue.self, forKey: .target)
            self.target = issue
        }

        self.targetUrl = try container.decode(String.self, forKey: .targetUrl)
        self.body = try container.decode(String.self, forKey: .body)
        self.state = try container.decode(State.self, forKey: .state)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
    }
}

class Project: Decodable {

    let id: Int
    let name: String
    let nameWithNamespace: String
    let path: String
    let pathWithNamespace: String

    enum CodingKeys: String, CodingKey {

        case id
        case name
        case nameWithNamespace = "name_with_namespace"
        case path
        case pathWithNamespace = "path_with_namespace"
    }
}

class Milestone: Decodable {

    let id: Int
    let iid: Int
    let projectId: Int
    let title: String
    let description: String
    let state: String //should be an enum?
    let createdAt: Date
    let updatedAt: Date
    let dueDate: Date?

    enum CodingKeys: String, CodingKey {

        case id
        case iid
        case projectId = "project_id"
        case title
        case description
        case state
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case dueDate = "due_date"
    }
}

extension Todo: Hashable {

    var hashValue: Int { return self.id }

    static func == (lhs: Todo, rhs: Todo) -> Bool {

        return lhs.id == rhs.id
    }
}
