//
//  Target.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 25/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation
 
class Target: Decodable {

    let id: Int
    let iid: Int
    let projectId: Int
    let title: String
    let description: String
    let createdAt: Date
    let updatedAt: Date
    let closedAt: Date?
    let closedBy: User?
    let upvotes: Int
    let downvotes: Int
    let author: User
    let assignee: User?
    let assignees: [User]?
    let labels: [String]?
    let milestone: Milestone?
    let userNotesCount: Int
    let webUrl: String

    private enum CodingKeys: String, CodingKey {

        case id
        case iid
        case projectId = "project_id"
        case title
        case description
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case closedAt = "closed_at"
        case closedBy = "closed_by"
        case upvotes
        case downvotes
        case author
        case assignee
        case assignees
        case labels
        case milestone
        case userNotesCount = "user_notes_count"
        case webUrl = "web_url"
    }
}

final class MergeRequest: Target {

    enum State: String, Decodable {

        case opened
        case closed
        case locked
        case merged
    }

    enum Status: String, Decodable {

        case canBeMerged = "can_be_merged"
        case cannotBeMerged = "cannot_be_merged"
        case unchecked
    }

    let state: State
    let mergedBy: User?
    let mergedAt: Date?
    let targetBranch: String
    let sourceBranch: String
    let sourceProjectId: Int
    let targetProjectId: Int
    let workInProgress: Bool
    let mergeWhenPipelineSucceeds: Bool
    let mergeStatus: Status

    private enum CodingKeys: String, CodingKey {

        case state
        case mergedBy = "merged_by"
        case mergedAt = "merged_at"
        case targetBranch = "target_branch"
        case sourceBranch = "source_branch"
        case sourceProjectId = "source_project_id"
        case targetProjectId = "target_project_id"
        case workInProgress = "work_in_progress"
        case mergeWhenPipelineSucceeds = "merge_when_pipeline_succeeds"
        case mergeStatus = "merge_status"
    }

    required init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.state = try container.decode(State.self, forKey: .state)
        self.mergedBy = try container.decodeIfPresent(User.self, forKey: .mergedBy)
        self.mergedAt = try container.decodeIfPresent(Date.self, forKey: .mergedAt)
        self.targetBranch = try container.decode(String.self, forKey: .targetBranch)
        self.sourceBranch = try container.decode(String.self, forKey: .sourceBranch)
        self.sourceProjectId = try container.decode(Int.self, forKey: .sourceProjectId)
        self.targetProjectId = try container.decode(Int.self, forKey: .targetProjectId)
        self.workInProgress = try container.decode(Bool.self, forKey: .workInProgress)
        self.mergeWhenPipelineSucceeds = try container.decode(Bool.self, forKey: .mergeWhenPipelineSucceeds)
        self.mergeStatus = try container.decode(Status.self, forKey: .mergeStatus)

        try super.init(from: decoder)
    }
}

final class Issue: Target {

    enum State: String, Decodable {

        case opened
        case closed
    }

    let state: State

    private enum CodingKeys: String, CodingKey {

        case state
        case assignees
    }

    required init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.state = try container.decode(State.self, forKey: .state)

        try super.init(from: decoder)
    }
}

extension Target: Equatable {

    var hashValue: Int { return self.id }

    static func == (lhs: Target, rhs: Target) -> Bool {

        return lhs.id == rhs.id
    }
}
