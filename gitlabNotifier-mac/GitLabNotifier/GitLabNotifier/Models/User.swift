//
//  User.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 22/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

class User: Decodable {

    let id: Int
    let username: String
    let name: String
    let state: String //should be an enum?
    let avatarUrl: String?
    let webUrl: String

    enum CodingKeys: String, CodingKey {

        case id
        case username
        case name
        case state
        case avatarUrl = "avatar_url"
        case webUrl = "web_url"
    }
}
