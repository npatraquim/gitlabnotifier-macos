//
//  TodoView+Configuration.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 11/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

extension TodoView {

    func configure(todo: Todo, animateProgressIndicator: Bool = false) {

        self.progressIndicator.isHidden = true
        self.progressIndicator.stopAnimation(nil)
        self.doneButton.isHidden = false

        self.titleLabel.stringValue = todo.target.title
        self.projectPathLabel.stringValue = todo.project.path
        self.usernameLabel.stringValue = todo.target.author.username
        self.dateLabel.stringValue = DateFormatterHelper.yyyyMMDDHourDate(date: todo.createdAt)

        //if the merge request is closed or merged we don't show the labels.
        if let mergeRequestTodo = todo.target as? MergeRequest,
            let statusViewStyle = StatusViewStyle(rawValue: mergeRequestTodo.state.rawValue) {

            let statusView = StatusView(title: mergeRequestTodo.state.rawValue, statusViewStyle: statusViewStyle)

            self.stackView.addArrangedSubview(statusView)

            return
        }

        todo.target.labels?.forEach {

            guard let statusViewStyle = StatusViewStyle(rawValue: $0) else { return }

            let statusView = StatusView(title: $0, statusViewStyle: statusViewStyle)

            self.stackView.addArrangedSubview(statusView)
        }
    }
}
