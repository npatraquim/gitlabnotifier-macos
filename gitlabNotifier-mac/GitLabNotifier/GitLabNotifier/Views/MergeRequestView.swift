//
//  MergeRequestView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 19/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class MergeRequestView: NSView {

    private enum Constants {

        static let dateLabelFontSize: CGFloat = 10.5
        static let dateLabelLeadingConstant: CGFloat = 10.0

        static let stackViewLeadingConstant: CGFloat = 10.0
        static let stackViewSpacing: CGFloat = 4.0
        static let stackViewTrailingConstant: CGFloat = 10.0
        static let stackViewTopConstant: CGFloat = 5.0

        static let thumbsDownImageLeadingConstant: CGFloat = 10.0

        static let thumbsImageSize: CGFloat = 20.0
        static let thumbsLabelFontSize: CGFloat = 10.5

        static let thumbsUpImageBottomConstant: CGFloat = 5.0
        static let thumbsUpImageLeadingConstant: CGFloat = 20.0
        static let thumbsUpImageTopConstant: CGFloat = 5.0

        static let titleLabelFontSize: CGFloat = 12.0
        static let titleLabelLeadingConstant: CGFloat = 10.0
        static let titleLabelTrailingConstant: CGFloat = 10.0
        static let titleLabelTopConstant: CGFloat = 5.0

        static let viewWidthAnchor: CGFloat = 450.0
    }

    let dateLabel = NSTextField(frame: NSRect.zero)
    let stackView = NSStackView(frame: NSRect.zero)
    let titleLabel = NSTextField(frame: NSRect.zero)
    let thumbsDownImage = NSImageView(image: NSImage(named: "thumbsDown")!)
    let thumbsDownLabel = NSTextField(frame: NSRect.zero)
    let thumbsUpImage = NSImageView(image: NSImage(named: "thumbsUp")!)
    let thumbsUpLabel = NSTextField(frame: NSRect.zero)

    unowned private let theme: ThemeProtocol

    static var identifier: NSUserInterfaceItemIdentifier {

        return NSUserInterfaceItemIdentifier(rawValue: "\(self)")
    }

    init(theme: ThemeProtocol) {

        self.theme = theme

        super.init(frame: .zero)

        self.configureView()
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Layout

private extension MergeRequestView {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubview(self.dateLabel)
        self.addSubview(self.titleLabel)
        self.addSubview(self.stackView)
        self.addSubview(self.thumbsDownImage)
        self.addSubview(self.thumbsDownLabel)
        self.addSubview(self.thumbsUpImage)
        self.addSubview(self.thumbsUpLabel)
    }

    func defineSubviewConstraints() {

        let _ = self.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.titleLabelLeadingConstant),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.titleLabelTrailingConstant),
            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.titleLabelTopConstant),

            self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.stackViewLeadingConstant),
            self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.stackViewTrailingConstant),
            self.stackView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: Constants.stackViewTopConstant),

            self.dateLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.dateLabelLeadingConstant),
            self.dateLabel.bottomAnchor.constraint(equalTo: self.thumbsUpImage.bottomAnchor),

            self.thumbsUpImage.topAnchor.constraint(equalTo: self.stackView.bottomAnchor, constant: Constants.thumbsUpImageTopConstant),
            self.thumbsUpImage.leadingAnchor.constraint(equalTo: self.dateLabel.trailingAnchor, constant: Constants.thumbsUpImageLeadingConstant),
            self.thumbsUpImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.thumbsUpImageBottomConstant),
            self.thumbsUpImage.widthAnchor.constraint(equalToConstant: Constants.thumbsImageSize),
            self.thumbsUpImage.heightAnchor.constraint(equalToConstant: Constants.thumbsImageSize),

            self.thumbsUpLabel.leadingAnchor.constraint(equalTo: self.thumbsUpImage.trailingAnchor),
            self.thumbsUpLabel.centerYAnchor.constraint(equalTo: self.thumbsUpImage.centerYAnchor),

            self.thumbsDownImage.leadingAnchor.constraint(equalTo: self.thumbsUpLabel.trailingAnchor, constant: Constants.thumbsDownImageLeadingConstant),
            self.thumbsDownImage.centerYAnchor.constraint(equalTo: self.thumbsUpImage.centerYAnchor),
            self.thumbsDownImage.widthAnchor.constraint(equalToConstant: Constants.thumbsImageSize),
            self.thumbsDownImage.heightAnchor.constraint(equalToConstant: Constants.thumbsImageSize),

            self.thumbsDownLabel.leadingAnchor.constraint(equalTo: self.thumbsDownImage.trailingAnchor),
            self.thumbsDownLabel.centerYAnchor.constraint(equalTo: self.thumbsDownImage.centerYAnchor),

            self.widthAnchor.constraint(equalToConstant: Constants.viewWidthAnchor)
        ])

        self.stackView.setContentHuggingPriority(.required, for: .vertical)
    }

    func configureSubviews() {

        self.titleLabel.usesSingleLineMode = false
        self.titleLabel.isEditable = false
        self.titleLabel.isBordered = false
        self.titleLabel.lineBreakMode = .byWordWrapping
        self.titleLabel.backgroundColor = .clear
        self.titleLabel.font = NSFont.systemFont(ofSize: Constants.titleLabelFontSize, weight: .semibold)

        self.dateLabel.isEditable = false
        self.dateLabel.isBordered = false
        self.dateLabel.backgroundColor = .clear
        self.dateLabel.font = NSFont.systemFont(ofSize: Constants.dateLabelFontSize, weight: .semibold)
        self.dateLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.thumbsUpLabel.isEditable = false
        self.thumbsUpLabel.isBordered = false
        self.thumbsUpLabel.backgroundColor = .clear
        self.thumbsUpLabel.font = NSFont.systemFont(ofSize: Constants.thumbsLabelFontSize, weight: .semibold)
        self.thumbsUpLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.thumbsDownLabel.isEditable = false
        self.thumbsDownLabel.isBordered = false
        self.thumbsDownLabel.backgroundColor = .clear
        self.thumbsDownLabel.font = NSFont.systemFont(ofSize: Constants.thumbsLabelFontSize, weight: .semibold)
        self.thumbsDownLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.stackView.spacing = Constants.stackViewSpacing
        self.stackView.alignment = .left
        self.stackView.orientation = .horizontal
    }
}
