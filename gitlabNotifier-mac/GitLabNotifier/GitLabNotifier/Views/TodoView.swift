//
//  TodoView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 07/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

protocol TodoViewDelegate: class {

    func doneButtonWasPressed(for todoView: TodoView)
}

class TodoView: NSView {

    private enum Constants {

        static let dateLabelFontSize: CGFloat = 10.5
        static let dateLabelLeadingConstant: CGFloat = 5.0
        static let dateLabelTrailingConstant: CGFloat = 10.0

        static let doneButtonTitle = "Done"
        static let doneButtonTrailingConstant: CGFloat = 10.0

        static let progressIndicatorSize: CGFloat = 20.0

        static let projectPathLabelBottomConstant: CGFloat = 5.0
        static let projectPathLabelFontSize: CGFloat = 10.5
        static let projectPathLabelLeadingConstant: CGFloat = 10.0
        static let projectPathLabelTopConstant: CGFloat = 5.0

        static let separatorViewMargins: CGFloat = 20.0
        static let separatorViewHeight: CGFloat = 1.0

        static let stackViewLeadingConstant: CGFloat = 10.0
        static let stackViewSpacing: CGFloat = 4.0
        static let stackViewTrailingConstant: CGFloat = 10.0
        static let stackViewTopConstant: CGFloat = 5.0

        static let thumbsDownImageLeadingConstant: CGFloat = 10.0

        static let thumbsImageSize: CGFloat = 20.0
        static let thumbsLabelFontSize: CGFloat = 10.5

        static let thumbsUpImageBottomConstant: CGFloat = 5.0
        static let thumbsUpImageLeadingConstant: CGFloat = 20.0
        static let thumbsUpImageTopConstant: CGFloat = 5.0

        static let titleLabelFontSize: CGFloat = 12.0
        static let titleLabelLeadingConstant: CGFloat = 10.0
        static let titleLabelTrailingConstant: CGFloat = 10.0
        static let titleLabelTopConstant: CGFloat = 5.0

        static let usernameLabelFontSize: CGFloat = 10.5
        static let usernameLabelLeadingConstant: CGFloat = 5.0

        static let viewWidthAnchor: CGFloat = 450.0
    }

    let dateLabel = NSTextField(frame: .zero)
    let doneButton = NSButton(frame: .zero)
    let progressIndicator = NSProgressIndicator(frame: .zero)
    let projectPathLabel = NSTextField(frame: .zero)
    let stackView = NSStackView(frame: .zero)
    let titleLabel = NSTextField(frame: .zero)
    let usernameLabel = NSTextField(frame: .zero)

    weak var delegate: TodoViewDelegate?
    unowned private let theme: ThemeProtocol

    static var identifier: NSUserInterfaceItemIdentifier {

        return NSUserInterfaceItemIdentifier(rawValue: "\(self)")
    }

    init(theme: ThemeProtocol) {

        self.theme = theme

        super.init(frame: .zero)

        self.configureView()
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Layout

private extension TodoView {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubview(self.titleLabel)
        self.addSubview(self.stackView)
        self.addSubview(self.projectPathLabel)
        self.addSubview(self.usernameLabel)
        self.addSubview(self.dateLabel)
        self.addSubview(self.doneButton)
        self.addSubview(self.progressIndicator)
    }

    func defineSubviewConstraints() {

        let _ = self.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([

            self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.titleLabelLeadingConstant),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.doneButton.leadingAnchor, constant: -Constants.titleLabelTrailingConstant),
            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.titleLabelTopConstant),

            self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.stackViewLeadingConstant),
            self.stackView.trailingAnchor.constraint(equalTo: self.doneButton.leadingAnchor, constant: -Constants.stackViewTrailingConstant),
            self.stackView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: Constants.stackViewTopConstant),

            self.projectPathLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.projectPathLabelLeadingConstant),
            self.projectPathLabel.topAnchor.constraint(equalTo: self.stackView.bottomAnchor, constant: Constants.projectPathLabelTopConstant),
            self.projectPathLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.projectPathLabelBottomConstant),

            self.usernameLabel.leadingAnchor.constraint(equalTo: self.projectPathLabel.trailingAnchor, constant: Constants.usernameLabelLeadingConstant),
            self.usernameLabel.centerYAnchor.constraint(equalTo: self.projectPathLabel.centerYAnchor),

            self.dateLabel.leadingAnchor.constraint(equalTo: self.usernameLabel.trailingAnchor, constant: Constants.dateLabelLeadingConstant),
            self.dateLabel.centerYAnchor.constraint(equalTo: self.usernameLabel.centerYAnchor),
            self.dateLabel.trailingAnchor.constraint(equalTo: self.doneButton.leadingAnchor, constant: -Constants.dateLabelTrailingConstant),

            self.doneButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.doneButtonTrailingConstant),
            self.doneButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),

            self.progressIndicator.centerXAnchor.constraint(equalTo: self.doneButton.centerXAnchor),
            self.progressIndicator.centerYAnchor.constraint(equalTo: self.doneButton.centerYAnchor),
            self.progressIndicator.widthAnchor.constraint(equalToConstant: Constants.progressIndicatorSize),
            self.progressIndicator.heightAnchor.constraint(equalToConstant: Constants.progressIndicatorSize),

            self.widthAnchor.constraint(equalToConstant: Constants.viewWidthAnchor)
        ])

        self.doneButton.setContentHuggingPriority(.required, for: .horizontal)
        self.projectPathLabel.setContentHuggingPriority(.required, for: .horizontal)
        self.usernameLabel.setContentHuggingPriority(.required, for: .horizontal)
        self.stackView.setContentHuggingPriority(.required, for: .vertical)
    }

    func configureSubviews() {

        self.titleLabel.usesSingleLineMode = false
        self.titleLabel.isEditable = false
        self.titleLabel.isBordered = false
        self.titleLabel.lineBreakMode = .byWordWrapping
        self.titleLabel.backgroundColor = .clear
        self.titleLabel.font = NSFont.systemFont(ofSize: Constants.titleLabelFontSize, weight: .semibold)

        self.projectPathLabel.isEditable = false
        self.projectPathLabel.isBordered = false
        self.projectPathLabel.backgroundColor = .clear
        self.projectPathLabel.font = NSFont.systemFont(ofSize: Constants.projectPathLabelFontSize, weight: .semibold)
        self.projectPathLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.usernameLabel.isEditable = false
        self.usernameLabel.isBordered = false
        self.usernameLabel.backgroundColor = .clear
        self.usernameLabel.font = NSFont.systemFont(ofSize: Constants.usernameLabelFontSize, weight: .semibold)
        self.usernameLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.dateLabel.isEditable = false
        self.dateLabel.isBordered = false
        self.dateLabel.backgroundColor = .clear
        self.dateLabel.font = NSFont.systemFont(ofSize: Constants.dateLabelFontSize, weight: .regular)
        self.dateLabel.textColor = self.theme.themeColor == .light ? .gray : .white

        self.doneButton.bezelStyle = .regularSquare
        self.doneButton.isBordered = true
        self.doneButton.wantsLayer = true
        self.doneButton.layer?.backgroundColor = .clear
        self.doneButton.title = Constants.doneButtonTitle
        self.doneButton.target = self
        self.doneButton.action = #selector(doneButtonWasTapped)

        self.progressIndicator.style = .spinning
        self.progressIndicator.isHidden = true

        self.stackView.spacing = Constants.stackViewSpacing
        self.stackView.alignment = .left
        self.stackView.orientation = .horizontal
    }
}

extension TodoView {

    @objc func doneButtonWasTapped() {

        self.progressIndicator.isHidden = false
        self.progressIndicator.startAnimation(nil)
        self.doneButton.isHidden = true

        self.delegate?.doneButtonWasPressed(for: self)
    }
}
