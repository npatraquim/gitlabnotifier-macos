//
//  MergeRequestView+Configuration.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 19/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

extension MergeRequestView {

    func configure(mergeRequest: MergeRequest) {

        self.titleLabel.stringValue = mergeRequest.title
        self.dateLabel.stringValue = DateFormatterHelper.yyyyMMDDHourDate(date: mergeRequest.createdAt)
        self.thumbsUpLabel.stringValue = String(mergeRequest.upvotes)
        self.thumbsDownLabel.stringValue = String(mergeRequest.downvotes)

        mergeRequest.labels?.forEach {

            guard let statusViewStyle = StatusViewStyle(rawValue: $0) else { return }

            let statusView = StatusView(title: $0, statusViewStyle: statusViewStyle)

            self.stackView.addArrangedSubview(statusView)
        }
    }
}
