//
//  HeaderView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 08/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class HeaderView: NSView {

    private enum Constants {

        static let titleLabelMargins: CGFloat = 10.0
        static let titleLabelFontSize: CGFloat = 20.0
    }

    private let titleLabel: NSTextField = NSTextField(frame: NSRect.zero)

    unowned private let theme: ThemeProtocol

    init(title: String, theme: ThemeProtocol) {

        self.theme = theme

        super.init(frame: NSRect.zero)

        self.titleLabel.stringValue = title
        self.configureView()
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(title: String) {

        self.titleLabel.stringValue = title
    }
}

//MARK: Layout

private extension HeaderView {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubview(self.titleLabel)
    }

    func defineSubviewConstraints() {

        let _ = self.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.titleLabelMargins),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.titleLabelMargins),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.titleLabelMargins),
            self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.titleLabelMargins),
        ])
    }

    func configureSubviews() {

        self.titleLabel.maximumNumberOfLines = 1
        self.titleLabel.isEditable = false
        self.titleLabel.isBordered = false
        self.titleLabel.font = NSFont.systemFont(ofSize: Constants.titleLabelFontSize, weight: .bold)
        self.titleLabel.textColor = self.theme.themeColor == .light ? .gray : .white
        self.titleLabel.backgroundColor = .clear
    }
}
