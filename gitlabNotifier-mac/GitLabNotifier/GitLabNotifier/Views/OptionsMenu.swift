//
//  OptionsMenu.swift
//  GitLabNotifier
//
//  Created by Francisco Amado on 12/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation
import AppKit
import LaunchAtLogin

class OptionsMenu: NSMenu {

    let navigator: SettingsNavigationProtocol

    let aboutItem: NSMenuItem = {
        let item = NSMenuItem(title: "About", action: #selector(aboutAction), keyEquivalent: "a")
        return item
    }()

    let launchItem: NSMenuItem = {
        let item = NSMenuItem(title: "Launch at Login", action: #selector(OptionsMenu.launchAction(sender:)), keyEquivalent: "")
        item.state = LaunchAtLogin.isEnabled == true ? .on : .off
        return item
    }()

    let quitItem: NSMenuItem = {
        let item = NSMenuItem(title: "Quit", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q")
        return item
    }()

    let settingsItem: NSMenuItem = {
        let item = NSMenuItem(title: "Settings", action: #selector(OptionsMenu.settingsAction(sender:)), keyEquivalent: "s")
        return item
    }()

    init(navigator: SettingsNavigationProtocol) {

        self.navigator = navigator

        super.init(title: "Options")

        setup()
    }

    required init(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension OptionsMenu {

    func setup() {

        aboutItem.target = self
        settingsItem.target = self
        [aboutItem, settingsItem, quitItem].forEach(addItem)
    }

    @objc func aboutAction(sender: Any) {}

    @objc func settingsAction(sender: Any) {
        #if DEBUG
            self.navigator.navigateToSettings()
        #endif
    }

    @objc func launchAction(sender: Any) {

        LaunchAtLogin.isEnabled.toggle()
        launchItem.state = LaunchAtLogin.isEnabled == true ? .on : .off
    }
}
