//
//  TableView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 19/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

protocol TableViewDelegate: class {

    func height(tableView: NSTableView, height: CGFloat)
}

class TableView: NSTableView {

    weak var heightDelegate: TableViewDelegate?

    override func layout() {

        super.layout()

        self.heightDelegate?.height(tableView: self, height: self.intrinsicContentSize.height)
    }
}
