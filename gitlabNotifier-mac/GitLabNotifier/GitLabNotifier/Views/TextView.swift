//
//  TextView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 29/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class TextView: NSView {

    private enum Constants {

        static let titleLabelLeftConstraint: CGFloat = 10.0
        static let titleLabelRightConstraint: CGFloat = 10.0
        static let titleLabelTopConstraint: CGFloat = 10.0
        static let titleLabelBottomConstraint: CGFloat = 10.0
        static let titleLabelFontSize: CGFloat = 14.0
        static let viewWidthConstraint: CGFloat = 450.0
    }

    private let titleLabel: NSTextField = NSTextField(frame: NSRect.zero)

    init(title: String) {

        super.init(frame: NSRect.zero)

        self.titleLabel.stringValue = title
        self.configureView()
    }

    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(title: String) {

        self.titleLabel.stringValue = title
    }
}

//MARK: Layout

private extension TextView {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubview(self.titleLabel)
    }

    func defineSubviewConstraints() {

        let _ = self.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.titleLabelTopConstraint),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.titleLabelLeftConstraint),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.titleLabelRightConstraint),
            self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.titleLabelBottomConstraint),

            self.widthAnchor.constraint(equalToConstant: Constants.viewWidthConstraint)
        ])
    }

    func configureSubviews() {

        self.titleLabel.maximumNumberOfLines = 0
        self.titleLabel.isEditable = false
        self.titleLabel.isBordered = false
        self.titleLabel.font = NSFont.systemFont(ofSize: Constants.titleLabelFontSize, weight: .bold)
        self.titleLabel.backgroundColor = .clear
        self.titleLabel.alignment = .center
    }
}
