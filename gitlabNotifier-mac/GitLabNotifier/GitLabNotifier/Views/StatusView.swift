//
//  StatusView.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 10/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

enum StatusViewStyle: String {

    case low
    case medium
    case high
    case buildOk = "BUILD OK"
    case buildInProgress = "BUILD IN PROGRESS 🏃‍♂️"
    case buildFailed = "BUILD FAILED"
    case merged
    case closed

    var color: NSColor {

        switch self {
        case .low:
            return NSColor(red: 120/255.0, green: 214/255.0, blue: 12/255.0, alpha: 1.0)

        case .medium:
            return NSColor(red: 244/255.0, green: 223/255.0, blue: 66/255.0, alpha: 1.0)

        case .high:
            return NSColor(red: 209/255.0, green: 35/255.0, blue: 73/255.0, alpha: 1.0)

        case .buildOk:
            return NSColor(red: 92/255.0, green: 184/255.0, blue: 92/255.0, alpha: 1.0)

        case .buildInProgress:
            return NSColor(red: 252/255.0, green: 201/255.0, blue: 0.0, alpha: 1.0)

        case .buildFailed,
             .closed:
            return .red

        case .merged:
            return NSColor(red: 31/255.0, green: 120/255.0, blue: 209/255.0, alpha: 1.0)
        }
    }
}

class StatusView: NSView {

    private enum Constants {

        static let titleLabelFontSize: CGFloat = 10.5
        static let titleLabelMargins: CGFloat = 2.0
    }

    private let titleLabel: NSTextField = NSTextField(frame: NSRect.zero)
    private let statusViewStyle: StatusViewStyle

    init(title: String, statusViewStyle: StatusViewStyle) {

        self.titleLabel.stringValue = title
        self.statusViewStyle = statusViewStyle

        super.init(frame: .zero)

        self.configureView()
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: Layout

private extension StatusView {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.addSubview(self.titleLabel)
    }

    func defineSubviewConstraints() {

        let _ = self.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.titleLabelMargins),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.titleLabelMargins),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.titleLabelMargins),
            self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.titleLabelMargins),
        ])
    }

    func configureSubviews() {

        self.wantsLayer = true
        self.layer?.backgroundColor = self.statusViewStyle.color.cgColor

        self.titleLabel.maximumNumberOfLines = 1
        self.titleLabel.isEditable = false
        self.titleLabel.isBordered = false
        self.titleLabel.font = NSFont.systemFont(ofSize: Constants.titleLabelFontSize, weight: .regular)
        self.titleLabel.textColor = .white
        self.titleLabel.backgroundColor = .clear
    }
}
