//
//  NetworkError.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 20/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

public enum NetworkingErrorCodes: Int {

    // Request Error
    case invalidURL = 20000
    case queryStringInvalidatesURL = 20001

    // Response Error
    case badData = 30001

    // General
    case general = 50000
}

public enum NetworkError: CustomNSError {

    // Request Error
    case invalidURL
    case queryStringInvalidatesURL

    // Response Error
    case badData

    // General
    case general

    /// The domain of the error.
    public static var errorDomain: String {

        return ""
    }

    /// The error code within the given domain.
    public var errorCode: Int {

        switch self {

        case .invalidURL:
            return NetworkingErrorCodes.invalidURL.rawValue
        case .queryStringInvalidatesURL:
            return NetworkingErrorCodes.queryStringInvalidatesURL.rawValue
        case .badData:
            return NetworkingErrorCodes.badData.rawValue
        case .general:
            return NetworkingErrorCodes.general.rawValue
        }
    }

    /// The user-info dictionary.
    public var errorUserInfo: [String: Any] {

        switch self {

        case .invalidURL:
            return [NSLocalizedDescriptionKey: "Can't convert given URL to URLComponents"]
        case .queryStringInvalidatesURL:
            return [NSLocalizedDescriptionKey: "Invalid url with received queryString parameters"]
        case .badData:
            return [NSLocalizedDescriptionKey: "Returned data isn't parsable"]
        case .general:
            return [NSLocalizedDescriptionKey: "General error occured"]
        }
    }
}
