//
//  GitLabAPI.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 21/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

import RxSwift

final class GitLabAPI {

    private enum Constants {

        static let baseUrl = URL(string: "https://gitlab.fftech.info/api/v4")!

        static let todoPath = "/todos"
        static let doneTodoPath = "/todos/%d/mark_as_done"
        static let mergeRequestPath = "/merge_requests"
    }

    unowned private let account: AccountProtocol
    private let network: Network

    init(account: AccountProtocol,
         network: Network) {

        self.account = account
        self.network = network
    }
}

extension GitLabAPI {

    enum Resource {

        case todos
        case doneTodo(Int)
        case mergeRequest([String: String]?)

        var verb: HTTPVerb {

            switch self {

            case .todos:
                return .get(nil)

            case .doneTodo:
                return .post(nil)

            case .mergeRequest(let params):

                return .get(params)
            }
        }

        var url: URL {

            return Constants.baseUrl.appendingPathComponent(self.pathComponent)
        }

        private var pathComponent: String {

            switch self {

            case .todos:
                return Constants.todoPath

            case .doneTodo(let id):
                return String(format: Constants.doneTodoPath, id)

            case .mergeRequest:
                return Constants.mergeRequestPath
            }
        }
    }
}

//MARK:- Requests

private extension GitLabAPI {

    func request<T: Decodable>(resource: Resource, token: String? = nil, completion: @escaping (NetworkingResult<T>) -> Void) {

        guard let privateToken = token ?? self.account.privateToken else { return }

        let task = NetworkTask<T>(url: resource.url,
                                  verb: resource.verb,
                                  headerFields: ["Private-Token": privateToken]) { urlRequest, urlResponde, result in

                                    completion(result)
        }

        self.network.request(task: task)
    }
}

//MARK:- Todos

extension GitLabAPI {

    func requestTodos(token: String? = nil, completion: @escaping ([Todo]?, Error?) -> Void) {

        self.request(resource: Resource.todos, token: token, completion: { (result: NetworkingResult<[Todo]>) in

            switch result {

            case .success(let object):
                completion(object, nil)

            case .failure(let error):
                completion(nil, error)
            }
        })
    }

    func doneToDo(id: Int, completion: @escaping (Todo?, Error?) -> Void) {

        self.request(resource: Resource.doneTodo(id), completion: { (result: NetworkingResult<Todo>) in

            switch result {

            case .success(let object):
                completion(object, nil)

            case .failure(let error):
                completion(nil, error)
            }
        })
    }
}

//MARK - Merge requests

extension GitLabAPI {

    func requestMergeRequests(params: [String: String], completion: @escaping ([MergeRequest]?, Error?) -> Void) {

        self.request(resource: Resource.mergeRequest(params), completion: { (result: NetworkingResult<[MergeRequest]>) in

            switch result {

            case .success(let object):
                completion(object, nil)

            case .failure(let error):
                completion(nil, error)
            }
        })
    }
}


