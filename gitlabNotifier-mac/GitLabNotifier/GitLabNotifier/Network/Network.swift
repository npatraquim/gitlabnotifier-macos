//
//  Network.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 13/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation
import RxSwift

public enum NetworkingResult<T> {

    case success(object: T)
    case failure(error: NetworkError)
}

class Network: NSObject {

    private let session: URLSession

    init(session: URLSession = URLSession(configuration: .default)) {

        self.session = session
    }

    public func request<T>(task: NetworkTask<T>) {

        guard let urlRequest = try? task.urlRequest() else {

            return
        }

        let queue = DispatchQueue.global(qos: task.qosClass)

        queue.async {

            let dataTask = self.dataTask(urlRequest: urlRequest,
                                         queue: queue,
                                         task: task)

            task.dataTask = dataTask

            dataTask.resume()
        }
    }

    func dataTask<T>(urlRequest: URLRequest,
                     queue: DispatchQueue,
                     task: NetworkTask<T>) -> URLSessionDataTask {

        let dataTask = self.session.dataTask(with: urlRequest) { data, response, error in

            queue.async {

                var result: NetworkingResult<T>

                guard error == nil,
                    let data = data else {

                    task.completion(urlRequest, response, .failure(error: .general))
                    return
                }

                do {

                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.dateDecodingStrategy = .customISO8601
                    let object = try jsonDecoder.decode(T.self, from: data)
                    result = .success(object: object)

                } catch {

                    result = .failure(error: .badData)
                }

                task.completion(urlRequest, response, result)
            }
        }

        return dataTask
    }
}

extension DateFormatter {

    static let iso8601Full: DateFormatter = {

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let iso8601AlmostFull: DateFormatter = {

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let iso8601Simple: DateFormatter = {

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")

        return formatter
    }()
}

extension JSONDecoder.DateDecodingStrategy {

    static let customISO8601 = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        if let date = DateFormatter.iso8601Full.date(from: string)
            ?? DateFormatter.iso8601AlmostFull.date(from: string)
            ?? DateFormatter.iso8601Simple.date(from: string) {

            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(string)")
    }
}
