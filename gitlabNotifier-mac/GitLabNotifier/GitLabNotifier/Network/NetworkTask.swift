//
//  NetworkTask.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 20/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

public class NetworkTask<T> where T: Decodable {

    public typealias NetworkTaskCompletion = (URLRequest?, URLResponse?, NetworkingResult<T>) -> Void

    var dataTask: URLSessionTask?

    let url: URL
    let verb: HTTPVerb
    let headerFields: [String: String]

    let timeout: TimeInterval
    let qosClass: DispatchQoS.QoSClass

    let context: [String: String]

    let completion: NetworkTaskCompletion

    // MARK: Lifecycle

    /// Constructor for `FNWNetworkTask` with default parameters
    ///
    /// - Parameters:
    ///   - url: Valid `URL` with just schema, domain and base path (e.g. https://domain.top/path/subpath).
    ///   - verb: `FNWHTTPVerb` for request, this contains the payload info. Defaults to `.get(.empty)`.
    ///   - headerFields: Header fields to be injected into request. Defaults to `[:]`.
    ///   - authentication: Optional object implementing `FNWAuthentication`, if nil is passed authentication will always succeed. Defaults to `nil`.
    ///   - timeout: Optional value for the `URLRequest`'s timeoutInterval. Defaults to `60`.
    ///   - qosClass: Dispatch queue class where the request will do its work (e.g. encoding and decoding processing). Defaults to `.userInitiated`.
    ///   - context: Optional dictionary for the context of this network task (e.g. caller framework or view controller that triggered this). Will be forwarded in `FNWNetworkingErrorDelegate`.
    ///   - completion: Closure for when the task finishes. It executes in whatever QoSClass passed as argument.
    public init(url: URL,
                verb: HTTPVerb,
                headerFields: [String: String] = [:],
                timeout: TimeInterval = 60,
                qosClass: DispatchQoS.QoSClass = .userInitiated,
                context: [String: String] = [:],
                completion: @escaping NetworkTaskCompletion) {

        self.url = url
        self.verb = verb
        self.headerFields = headerFields

        self.timeout = timeout
        self.qosClass = qosClass

        self.context = context

        self.completion = completion
    }
}

extension NetworkTask {

    func urlRequest() throws -> URLRequest {

        let url: URL

        switch self.verb {

        case .get(let query),
             .delete(let query),
             .post(let query),
             .put(let query),
             .patch(let query):

            if let query = query {

                url = try self.urlWithQueryString(parameters: query)

            } else {

                url = self.url
            }
        }

        var request = URLRequest(url: url)

        request.httpMethod = self.verb.rawValue

        self.headerFields.forEach { request.setValue($0.1, forHTTPHeaderField: $0.0) }

        return request
    }

    func urlWithQueryString(parameters: [String: String]) throws -> URL {

        if var urlComponents = URLComponents(url: self.url, resolvingAgainstBaseURL: false) {

            var queryItems = urlComponents.queryItems ?? []

            queryItems.append(contentsOf: parameters.map { URLQueryItem(name: $0, value: $1) })

            urlComponents.queryItems = queryItems

            if let url = urlComponents.url {

                return url

            } else {

                throw NetworkError.queryStringInvalidatesURL
            }

        } else {

            throw NetworkError.invalidURL
        }
    }
}
