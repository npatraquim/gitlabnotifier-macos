//
//  HTTPVerb.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 20/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

public enum HTTPVerb {

    case get([String: String]?)
    case post([String: String]?)
    case put([String: String]?)
    case patch([String: String]?)
    case delete([String: String]?)
}

extension HTTPVerb: RawRepresentable {

    public typealias RawValue = String

    public init?(rawValue: String) {

        switch rawValue {
        case "GET":
            self = .get(nil)
        case "POST":
            self = .post(nil)
        case "PUT":
            self = .put(nil)
        case "PATCH":
            self = .patch(nil)
        case "DELETE":
            self = .delete(nil)
        default:
            return nil
        }
    }

    public var rawValue: RawValue {

        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .patch:
            return "PATCH"
        case .delete:
            return "DELETE"
        }
    }
}
