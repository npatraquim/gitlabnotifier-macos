//
//  NotificationManager.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 17/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

protocol NotificationProtocol: class {

    func presentNotificationsIfNeeded(oldTodos: [Todo], newTodos: [Todo])
}

class NotificationManager {

    private let popoverProtocol: PopoverProtocol

    init(popoverProtocol: PopoverProtocol) {

        self.popoverProtocol = popoverProtocol
    }
}

extension NotificationManager: NotificationProtocol {

    func presentNotificationsIfNeeded(oldTodos: [Todo], newTodos: [Todo]) {

        guard popoverProtocol.popover.isShown == false else { return }

        let oldSet: Set<Todo> = Set(oldTodos)
        let newSet: Set<Todo> = Set(newTodos)

        let notificationsToPresent = newSet.subtracting(oldSet)

        notificationsToPresent.forEach { todo in

            let notification = NSUserNotification()
            notification.identifier = String(todo.id)
            notification.title = todo.target.title
            notification.informativeText = todo.project.path + " " + todo.target.author.username
            notification.soundName = NSUserNotificationDefaultSoundName

            NSUserNotificationCenter.default.deliver(notification)
        }
    }
}
