//
//  AccountManager.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 21/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation
import Security

protocol AccountProtocol: class {

    var privateToken: String? { get }

    func savePrivateToken(token: String)
    func cleanPrivateToken()
}

class AccountManager: AccountProtocol {

    private enum Constants {

        static let privateTokenKey = "PrivateToken"
    }

    var privateToken: String? {

        return TokenManager.token
    }

    init() {

        migrateToken()
    }

    func savePrivateToken(token: String) {

        _ = TokenManager.save(token: token)
    }

    func cleanPrivateToken() {

        TokenManager.cleanKeychain()
    }

    // Temporary action to Migrate the Token from the UserDefaults to the Keychain
    func migrateToken() {

        guard let oldToken = UserDefaults.standard.string(forKey: Constants.privateTokenKey) else {

            return
        }

        self.savePrivateToken(token: oldToken)
    }
}
