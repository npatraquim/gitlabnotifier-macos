//
//  ThemeManager.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 22/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

protocol ThemeProtocol: class {

    var themeColor: Theme { get }
}

enum Theme: Int {

    case dark
    case light
}

class ThemeManager: ThemeProtocol {

    var themeColor: Theme

    init() {

        self.themeColor = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") == "Dark" ? .dark : .light
    }
}
