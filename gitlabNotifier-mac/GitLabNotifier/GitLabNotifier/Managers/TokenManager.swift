//
//  TokenManager.swift
//  GitLabNotifier
//
//  Created by Francisco Amado on 12/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation
import Security
import os

class TokenManager {

    private static let identifier = "GitLabNotifier Token".data(using: .utf8)!

    class func save(token: String) -> Bool {

        guard let encodedToken = token.data(using: .utf8) else {

            os_log("Error encoding Token", log: .default, type: .error)
            return false
        }

        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: identifier,
                                    kSecValueData as String: encodedToken]

        let status: OSStatus = SecItemAdd(query as CFDictionary, nil)

        if status == errSecSuccess {

            return true

        } else if status == errSecDuplicateItem {

            return update(token: encodedToken)

        } else {

            os_log("Error saving Token", log: .default, type: .error)
            return false
        }
    }

    class func update(token: Data) -> Bool {

        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: identifier]

        let attributes: [String: Any] = [kSecValueData as String: token]

        let status: OSStatus = SecItemUpdate(query as CFDictionary, attributes as CFDictionary)

        if status == errSecSuccess {

            return true

        } else {

            os_log("Error upating Token", log: .default, type: .error)
            return false
        }
    }

    class var token: String? {

        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: identifier,
                                    kSecMatchLimit as String: kSecMatchLimitOne,
                                    kSecReturnData as String: kCFBooleanTrue]

        var item: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &item)

        guard status == errSecSuccess else {

            os_log("Error getting Token", log: .default, type: .error)
            return nil
        }

        guard let data = item as? Data else {

            os_log("Error getting Token Data", log: .default, type: .error)
            return nil
        }

        return String(data: data, encoding: String.Encoding.utf8) as String?
    }

    class func cleanKeychain() {

        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: identifier]

        let status = SecItemDelete(query as CFDictionary)

        if status != errSecSuccess {

            os_log("Error deleting Token", log: .default, type: .error)
        }
    }
}
