//
//  Router.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 22/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

protocol PopoverProtocol: class {

    var popover: NSPopover { get }
}

protocol LoginNavigationProtocol: class {

    @discardableResult
    func navigateToHome() -> NSViewController?
}

protocol SettingsNavigationProtocol: class {

    @discardableResult
    func navigateToSettings() -> NSViewController?
}

class Router: PopoverProtocol {

    var popover: NSPopover
    unowned private var factoryViewController: FactoryViewController

    init(factoryViewController: FactoryViewController) {

        self.popover = NSPopover()
        self.popover.behavior = .transient
        self.factoryViewController = factoryViewController
    }
}

extension Router {

    @discardableResult
    func navigateToLogin() -> NSViewController? {

        let loginViewController = self.factoryViewController.loginViewController()
        self.popover.contentViewController = loginViewController

        return loginViewController
    }
}

extension Router: LoginNavigationProtocol {

    @discardableResult
    func navigateToHome() -> NSViewController? {

        let homeViewController = self.factoryViewController.homeViewController()
        self.popover.contentViewController = homeViewController

        return homeViewController
    }
}

extension Router: SettingsNavigationProtocol {

    @discardableResult
    func navigateToSettings() -> NSViewController? {

        let window = NSWindow(contentViewController: self.factoryViewController.homeViewController()!)

        window.makeKeyAndOrderFront(nil)

        return nil
    }
}
