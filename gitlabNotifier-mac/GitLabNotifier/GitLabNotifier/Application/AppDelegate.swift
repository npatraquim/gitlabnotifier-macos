//
//  AppDelegate.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 01/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {

    private var core: Core?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application

        self.core = Core()

        if self.core?.accountManager.privateToken == nil {

            self.core?.router.navigateToLogin()

        } else {

            self.core?.router.navigateToHome()
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}
