//
//  String+Utils.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 18/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa
import Foundation

extension String {

    func heightWithConstrained(width: CGFloat, font: NSFont) -> CGFloat {

        let constraintRect = CGSize(width: width,
                                    height: CGFloat.greatestFiniteMagnitude)

        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: [.font: font],
                                            context: nil)

        return boundingBox.height
    }
}
