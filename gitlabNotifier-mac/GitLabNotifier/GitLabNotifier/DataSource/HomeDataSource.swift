//
//  HomeDataSource.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 15/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

import RxSwift

protocol HomeDataSourceDelegate: class {

    func homeDataSource(_ dataSource: HomeDataSource,
                        results: [HomeResult],
                        error: Error?)

    func homeDataSource(_ dataSource: HomeDataSource,
                        todoToDone: Todo,
                        doneTodo: Todo?,
                        error: Error?)
}

enum HomeResult {

    case header(String)
    case mergeRequest(MergeRequest)
    case text(String)
    case todo(Todo)
}

class HomeDataSource: DataSource {

    weak var delegate: HomeDataSourceDelegate?
    private let disposeBag = DisposeBag()
    private unowned let gitlabAPI: GitLabAPI

    init(gitlabAPI: GitLabAPI) {

        self.gitlabAPI = gitlabAPI
    }

    func requestAll() {

        var result: [HomeResult] = []

        self.requestTodos(gitlabAPI: self.gitlabAPI).asObservable()
            .flatMap { todos -> Single<[MergeRequest]> in

                result.append(.header("Todos"))

                if todos.count > 0 {

                    todos.forEach { result.append(.todo($0)) }

                } else {

                    result.append(.text("Nothing to show 😮. keep up the good work 💻"))
                }

                return self.requestMergeRequests(params: ["state":"opened"], gitlabAPI: self.gitlabAPI)

            }
            .subscribe(onNext: { mergeRequests in

                result.append(.header("Merge requests"))

                if mergeRequests.count > 0 {

                    mergeRequests.forEach { result.append(.mergeRequest($0)) }

                } else {

                    result.append(.text("Nothing to show 😮. keep up the good work 💻"))
                }

                DispatchQueue.main.async {

                    self.delegate?.homeDataSource(self, results: result, error: nil)
                }

            }, onError: { error in

                DispatchQueue.main.async {

                    result.append(.header("Todos"))
                    result.append(.text("Something bad happened 😨. Verify your connection and if you have access to gitlab.fftech.info"))
                    result.append(.header("Merge requests"))
                    result.append(.text("Something bad happened 😨. Verify your connection and if you have access to gitlab.fftech.info"))

                    self.delegate?.homeDataSource(self, results: result, error: error)
                }
            })
            .disposed(by: self.disposeBag)
    }

    func doneTodo(todo: Todo, gitlabAPI: GitLabAPI) {

        self.doneTodo(id: todo.id, gitlabAPI: self.gitlabAPI).asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { todoDone in

                self.delegate?.homeDataSource(self,
                                              todoToDone: todo,
                                              doneTodo: todoDone,
                                              error: nil)

            }, onError: { error in

                self.delegate?.homeDataSource(self,
                                              todoToDone: todo,
                                              doneTodo: nil,
                                              error: error)

            })
            .disposed(by: self.disposeBag)
    }
}
