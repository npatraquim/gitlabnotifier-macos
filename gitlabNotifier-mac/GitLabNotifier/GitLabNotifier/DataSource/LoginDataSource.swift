//
//  LoginDataSource.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 15/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

import RxSwift

protocol LoginDataSourceDelegate: class {

    func loginDataSource(_ dataSource: LoginDataSource,
                         success: Bool)
}

class LoginDataSource: DataSource {

    weak var delegate: LoginDataSourceDelegate?
    private var disposeBag = DisposeBag()
    unowned private let gitlabAPI: GitLabAPI

    init(gitlabAPI: GitLabAPI) {

        self.gitlabAPI = gitlabAPI
    }

    func verifyToken(token: String) {

        self.requestTodos(token: token, gitlabAPI: self.gitlabAPI).asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in

                return self.delegate?.loginDataSource(self, success: true)

            }, onError: { _ in

                return self.delegate?.loginDataSource(self, success: false)

            })
            .disposed(by: self.disposeBag)
    }
}
