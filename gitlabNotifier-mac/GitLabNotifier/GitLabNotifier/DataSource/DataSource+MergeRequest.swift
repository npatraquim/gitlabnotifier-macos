//
//  DataSource+MergeRequest.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 15/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

import RxSwift

extension DataSource {

    func requestMergeRequests(params: [String: String], gitlabAPI: GitLabAPI) -> Single<[MergeRequest]> {

        return Single.create { single in

            gitlabAPI.requestMergeRequests(params: params, completion: { (mergeRequests, error) in

                switch (mergeRequests, error) {

                case let (.some(mergeRequests), _):
                    single(.success(mergeRequests))

                case let (_, .some(error)):
                    single(.error(error))

                default:
                    single(.error(NSError()))
                }
            })

            return Disposables.create {}
        }
    }
}

