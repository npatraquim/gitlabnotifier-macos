//
//  DataSource+Todo.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 15/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

import RxSwift

extension DataSource {

    func requestTodos(token: String? = nil, gitlabAPI: GitLabAPI) -> Single<[Todo]> {

        return Single.create { single in

            gitlabAPI.requestTodos(token: token, completion: { (todos, error) in

                switch (todos, error) {

                case let (.some(todos), _):
                    single(.success(todos))

                case let (_, .some(error)):
                    single(.error(error))

                default:
                    single(.error(NSError()))
                }
            })

            return Disposables.create {}
        }
    }

    func doneTodo(id: Int, gitlabAPI: GitLabAPI) -> Single<Todo> {

        return Single.create { single in

            gitlabAPI.doneToDo(id: id, completion: { (todo, error) in

                switch (todo, error) {

                case let (.some(todo), _):
                    single(.success(todo))

                case let (_, .some(error)):
                    single(.error(error))

                default:
                    single(.error(NSError()))
                }
            })

            return Disposables.create {}
        }
    }
}
