//
//  StatusMenuHandler.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 01/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class StatusMenuHandler: NSObject {

    private var monitor: Any?
    private let popoverProtocol: PopoverProtocol & SettingsNavigationProtocol
    private let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    private let optionsMenu: OptionsMenu

    init(popover: PopoverProtocol & SettingsNavigationProtocol) {

        self.popoverProtocol = popover
        self.optionsMenu = OptionsMenu(navigator: popoverProtocol)

        super.init()

        let icon = NSImage(named: "statusIcon")
        icon?.isTemplate = true // best for dark mode

        if let button = statusItem.button {

            button.image = icon
            button.action = #selector(togglePopover(_:))
            button.sendAction(on: [.leftMouseUp, .rightMouseUp])
            button.target = self
        }

        if popoverProtocol.popover.isShown {

            self.startMonitor()
        }
    }

    @objc
    func togglePopover(_ sender: Any?) {

        if self.popoverProtocol.popover.isShown {

            closePopover(sender: sender)

        } else {

            showPopover(sender: sender)
        }
    }

    func showPopover(sender: Any?) {

        if NSApp.currentEvent?.type == NSEvent.EventType.rightMouseUp {

            self.statusItem.popUpMenu(self.optionsMenu)

        } else {

            if let button = statusItem.button {

                self.popoverProtocol.popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
            }

            self.startMonitor()
        }
    }

    func closePopover(sender: Any?) {

        self.popoverProtocol.popover.performClose(sender)
        self.stopMonitor()
    }

    private func startMonitor() {

        self.monitor = NSEvent.addGlobalMonitorForEvents(matching: [.leftMouseUp, .rightMouseUp], handler: { _ in

            if self.popoverProtocol.popover.isShown {

                self.closePopover(sender: nil)
            }
        })
    }

    private func stopMonitor() {

        guard let monitor = self.monitor else { return }

        NSEvent.removeMonitor(monitor)
        self.monitor = nil
    }
}
