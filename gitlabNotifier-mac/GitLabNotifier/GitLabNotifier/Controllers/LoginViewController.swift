//
//  LoginViewController.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 01/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

import RxSwift

final class LoginViewController: NSViewController {

    private enum Status {

        case first
        case second
    }

    private enum Constants {

        static let buttonBottomConstraint: CGFloat = 10.0
        static let buttonNextText = "Next"
        static let buttonOkText = "Ok"
        static let buttonTopConstraint: CGFloat = 10.0
        static let buttonWidthConstraint: CGFloat = 100.0

        static let errorLabelHeightConstraint: CGFloat = 22.0
        static let errorLabelGeneric = "Something bad happened. Please verify your personal token and try again."
        static let errorLabelMargins: CGFloat = 10.0
        static let errorLabelNoCharacters = "Please fulfill the text field above with your personal token."

        static let instructionsLabelMargins: CGFloat = 10.0
        static let instructions1: String = "Hi and welcome to the GitLab notifier. Here you can see and get notifications about your todos and merge requests. But firtsly you need to grant us access through your personal access token."
        static let instructions2: String = "Personal access tokens are the preferred way for third party applications and scripts to authenticate with the GitLab API. To create one: \n\n 1. Log in to your GitLab account. \n 2. Go to your Profile settings. \n 3. Go to Access tokens. \n 4. Choose a name and optionally an expiry date for the token. \n 5. Choose the desired scopes. \n 6. Click on Create personal access token. \n\nOnce you have it, write it down please."

        static let progressIndicatorHeightConstraint: CGFloat = 20.0
        static let progressIndicatorWidthConstraint: CGFloat = 20.0

        static let tokenInputTextFieldHeightConstraint: CGFloat = 22.0
        static let tokenInputTextFieldMargins: CGFloat = 10.0

        static let viewWidthConstraint: CGFloat = 300.0
    }

    private let instructionsLabel = NSTextField(frame: NSRect.zero)
    private let tokenInputTextField = NSTextField(frame: NSRect.zero)
    private let errorLabel = NSTextField(frame: NSRect.zero)
    private let button = NSButton(frame: NSRect.zero)
    private let progressIndicator = NSProgressIndicator(frame: NSRect.zero)

    private var tokenInputTextFieldHeightConstraint: NSLayoutConstraint?
    private var errorLabelHeightConstraint: NSLayoutConstraint?

    unowned private let account: AccountProtocol
    private let disposeBag = DisposeBag()
    unowned private let gitLabAPI: GitLabAPI
    private let loginDataSource: LoginDataSource
    unowned private let navigator: LoginNavigationProtocol
    private var status: Status = .first
    private var tokenInserted: String?

    init(account: AccountProtocol,
         gitLabAPI: GitLabAPI,
         navigator: LoginNavigationProtocol) {

        self.account = account
        self.gitLabAPI = gitLabAPI
        self.navigator = navigator

        self.loginDataSource = LoginDataSource(gitlabAPI: self.gitLabAPI)

        super.init(nibName: nil, bundle: nil)

        self.loginDataSource.delegate = self
    }

    override func loadView() {

        self.view = NSView()
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        // Do view setup here.
        self.configureView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Layout

extension LoginViewController {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.view.addSubview(self.instructionsLabel)
        self.view.addSubview(self.tokenInputTextField)
        self.view.addSubview(self.errorLabel)
        self.view.addSubview(self.button)
        self.view.addSubview(self.progressIndicator)
    }

    func defineSubviewConstraints() {

        let _ = self.view.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.instructionsLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: Constants.instructionsLabelMargins),
            self.instructionsLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Constants.instructionsLabelMargins),
            self.instructionsLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Constants.instructionsLabelMargins),

            self.tokenInputTextField.topAnchor.constraint(equalTo: self.instructionsLabel.bottomAnchor, constant: Constants.tokenInputTextFieldMargins),
            self.tokenInputTextField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Constants.tokenInputTextFieldMargins),
            self.tokenInputTextField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Constants.tokenInputTextFieldMargins),

            self.errorLabel.topAnchor.constraint(equalTo: self.tokenInputTextField.bottomAnchor),
            self.errorLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Constants.errorLabelMargins),
            self.errorLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Constants.errorLabelMargins),

            self.button.topAnchor.constraint(equalTo: self.errorLabel.bottomAnchor, constant: Constants.buttonTopConstraint),
            self.button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.button.widthAnchor.constraint(equalToConstant: Constants.buttonWidthConstraint),
            self.button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -Constants.buttonBottomConstraint),

            self.progressIndicator.centerXAnchor.constraint(equalTo: self.button.centerXAnchor),
            self.progressIndicator.centerYAnchor.constraint(equalTo: self.button.centerYAnchor),
            self.progressIndicator.widthAnchor.constraint(equalToConstant: Constants.progressIndicatorWidthConstraint),
            self.progressIndicator.heightAnchor.constraint(equalToConstant: Constants.progressIndicatorHeightConstraint),

            self.view.widthAnchor.constraint(equalToConstant: Constants.viewWidthConstraint)
        ])

        self.tokenInputTextFieldHeightConstraint = self.tokenInputTextField.heightAnchor.constraint(equalToConstant: 0.0)
        self.tokenInputTextFieldHeightConstraint?.isActive = true
        self.errorLabelHeightConstraint = self.errorLabel.heightAnchor.constraint(equalToConstant: 0.0)
        self.errorLabelHeightConstraint?.isActive = true

        self.button.setContentHuggingPriority(.required, for: .vertical)
    }

    func configureSubviews() {

        self.instructionsLabel.isEditable = false
        self.instructionsLabel.isBordered = false
        self.instructionsLabel.backgroundColor = .clear
        self.instructionsLabel.stringValue = Constants.instructions1

        self.tokenInputTextField.isHidden = true
        self.tokenInputTextField.alignment = .center

        self.errorLabel.isEditable = false
        self.errorLabel.isBordered = false
        self.errorLabel.backgroundColor = .clear
        self.errorLabel.textColor = .red

        self.button.title = Constants.buttonNextText
        self.button.bezelStyle = .regularSquare
        self.button.isBordered = false
        self.button.wantsLayer = true
        self.button.layer?.backgroundColor = .clear
        self.button.target = self
        self.button.action = #selector(LoginViewController.buttonWasTapped)

        self.progressIndicator.style = .spinning
        self.progressIndicator.isHidden = true
    }
}

//MARK: - Actions

extension LoginViewController {

    @objc
    func buttonWasTapped() {

        switch self.status {

        case .first:
            self.instructionsLabel.stringValue = Constants.instructions2
            self.tokenInputTextField.isHidden = false
            self.button.title = Constants.buttonOkText
            self.status = .second

            self.tokenInputTextFieldHeightConstraint?.constant = Constants.tokenInputTextFieldHeightConstraint

        case .second:
            let token = tokenInputTextField.stringValue.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            guard token != "" else {

                self.errorLabelHeightConstraint?.constant = Constants.errorLabelNoCharacters.heightWithConstrained(width: self.errorLabel.frame.width,
                                                                                                                       font: self.errorLabel.font!)
                self.errorLabel.stringValue = Constants.errorLabelNoCharacters
                return
            }

            self.errorLabelHeightConstraint?.constant = 0.0
            self.errorLabel.stringValue = ""

            self.tokenInputTextField.isEnabled = false
            self.button.isHidden = true
            self.progressIndicator.isHidden = false
            self.progressIndicator.startAnimation(nil)

            self.tokenInserted = token

            self.loginDataSource.verifyToken(token: token)
        }
    }
}

extension LoginViewController: LoginDataSourceDelegate {

    func loginDataSource(_ dataSource: LoginDataSource, success: Bool) {

        if success,
            let token = self.tokenInserted {

            self.account.savePrivateToken(token: token)
            self.navigator.navigateToHome()

        } else {

            self.tokenInputTextField.isEnabled = true
            self.button.isHidden = false
            self.progressIndicator.isHidden = true
            self.progressIndicator.startAnimation(nil)

            self.errorLabelHeightConstraint?.constant = Constants.errorLabelGeneric.heightWithConstrained(width: self.errorLabel.frame.width,
                                                                                                         font: self.errorLabel.font!)
            self.errorLabel.stringValue = Constants.errorLabelGeneric
        }
    }
}
