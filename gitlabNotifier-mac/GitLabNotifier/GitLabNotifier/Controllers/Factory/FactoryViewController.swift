//
//  FactoryViewController.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 22/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

class FactoryViewController {

    weak var core: Core?

    func loginViewController() -> NSViewController? {

        guard let account = self.core?.accountManager,
            let gitLabAPI = self.core?.gitLabAPI,
            let navigator = self.core?.router else {

            return nil
        }

        return LoginViewController(account: account,
                                   gitLabAPI: gitLabAPI,
                                   navigator: navigator)
    }

    func homeViewController() -> NSViewController? {

        guard let gitLabAPI = self.core?.gitLabAPI,
            let notification = self.core?.notificationManager,
            let popoverProtocol = self.core?.router,
            let theme = self.core?.theme else {

            return nil
        }

        return HomeViewController(gitLabAPI: gitLabAPI,
                                  notification: notification,
                                  popoverProtocol: popoverProtocol,
                                  theme: theme)
    }
}
