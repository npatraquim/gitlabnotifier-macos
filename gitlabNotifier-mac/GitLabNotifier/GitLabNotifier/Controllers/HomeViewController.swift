//
//  HomeViewController.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 07/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Cocoa

import RxSwift

final class HomeViewController: NSViewController {

    private enum Constants {

        static let columnIdentifier = "Column"

        static let infoLabelFontSize: CGFloat = 16.0
        static let infoLabelMargins: CGFloat = 10.0
        static let infoLabelErrorMessage = "Something bad happened 😨. Verify your connection and if you have access to gitlab.fftech.info"
        static let infoLabelNoResults = "Nothing to show 😮. keep up the good work 💻"

        static let scrollViewTableViewMinHeightConstant: CGFloat = 100.0
        static let scrollViewTableViewHeightConstant: CGFloat = 450.0
        static let scrollViewTableViewTopConstant: CGFloat = 10.0

        static let timerFireInterval: TimeInterval = 300.0

        static let updateButtonLeadingConstant: CGFloat = 50.0
        static let updateButtonTitle = "Update"
        static let updateButtonTopConstant: CGFloat = 20.0
        static let updateButtonTrailingConstant: CGFloat = 50.0

        static let updateProgressIndicatorSize: CGFloat = 20.0

        static let viewWidthAnchor: CGFloat = 450.0
    }

    private var progressIndicator = NSProgressIndicator()
    private var scrollViewTableView = NSScrollView()
    private var tableView = TableView(frame: .zero)
    private var updateButton = NSButton(frame: .zero)
    private var updateProgressIndicator = NSProgressIndicator()

    private var scrollViewTableViewHeightConstraint = NSLayoutConstraint()

    private var disposeBag = DisposeBag()
    unowned private var gitLabAPI: GitLabAPI
    private let homeDataSource: HomeDataSource
    unowned private let notification: NotificationProtocol
    unowned private let popoverProtocol: PopoverProtocol
    unowned private let theme: ThemeProtocol
    private var timer: Timer?

    private var todosToBeDone: [Todo]?

    private var results: [HomeResult] = []

    init(gitLabAPI: GitLabAPI,
         notification: NotificationProtocol,
         popoverProtocol: PopoverProtocol,
         theme: ThemeProtocol) {

        self.gitLabAPI = gitLabAPI
        self.homeDataSource = HomeDataSource(gitlabAPI: self.gitLabAPI)
        self.notification = notification
        self.popoverProtocol = popoverProtocol
        self.theme = theme

        super.init(nibName: nil, bundle: nil)

        self.homeDataSource.delegate = self

        self.timer = Timer.scheduledTimer(withTimeInterval: Constants.timerFireInterval, repeats: true, block: { _ in

            self.homeDataSource.requestAll()
        })
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {

        self.view = NSView()
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        self.configureView()

        self.homeDataSource.requestAll()
    }
}

//MARK:- Layout

private extension HomeViewController {

    func configureView() {

        self.addSubviews()
        self.defineSubviewConstraints()
        self.configureSubviews()
    }

    func addSubviews() {

        self.view.addSubview(self.progressIndicator)
        self.view.addSubview(self.scrollViewTableView)
        self.view.addSubview(self.updateButton)
        self.view.addSubview(self.updateProgressIndicator)
    }

    func defineSubviewConstraints() {

        let _ = self.view.subviews.map{ $0.translatesAutoresizingMaskIntoConstraints = false }

        NSLayoutConstraint.activate([

            self.updateButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: Constants.updateButtonTopConstant),
            self.updateButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: Constants.updateButtonLeadingConstant),
            self.updateButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -Constants.updateButtonTrailingConstant),

            self.updateProgressIndicator.centerYAnchor.constraint(equalTo: self.updateButton.centerYAnchor),
            self.updateProgressIndicator.centerXAnchor.constraint(equalTo: self.updateButton.centerXAnchor),
            self.updateProgressIndicator.heightAnchor.constraint(equalToConstant: Constants.updateProgressIndicatorSize),
            self.updateProgressIndicator.widthAnchor.constraint(equalToConstant: Constants.updateProgressIndicatorSize),

            self.progressIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.progressIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),

            self.scrollViewTableView.topAnchor.constraint(equalTo: self.updateButton.bottomAnchor, constant: Constants.scrollViewTableViewTopConstant),
            self.scrollViewTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.scrollViewTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.scrollViewTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),

            self.view.widthAnchor.constraint(equalToConstant: Constants.viewWidthAnchor),
        ])

        self.updateButton.setContentHuggingPriority(.required, for: .vertical)
        self.scrollViewTableViewHeightConstraint = self.scrollViewTableView.heightAnchor.constraint(equalToConstant: Constants.scrollViewTableViewHeightConstant)
        self.scrollViewTableViewHeightConstraint.isActive = true
    }

    func configureSubviews() {

        let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: Constants.columnIdentifier))
        column.width = 1
        self.tableView.addTableColumn(column)
        self.tableView.headerView = nil
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.heightDelegate = self
        self.tableView.usesAutomaticRowHeights = true
        self.tableView.backgroundColor = .clear
        self.tableView.selectionHighlightStyle = .none
        self.tableView.action = #selector(tableViewItemWasTapped)

        self.scrollViewTableView.documentView = self.tableView
        self.scrollViewTableView.drawsBackground = false

        self.progressIndicator.style = .spinning
        self.progressIndicator.startAnimation(nil)

        self.updateButton.bezelStyle = .regularSquare
        self.updateButton.isBordered = true
        self.updateButton.wantsLayer = true
        self.updateButton.layer?.backgroundColor = .clear
        self.updateButton.title = Constants.updateButtonTitle
        self.updateButton.target = self
        self.updateButton.action = #selector(updateButtonWasTapped)

        self.updateProgressIndicator.style = .spinning
        self.updateProgressIndicator.isHidden = true
    }
}

//MARK:- NSTableViewDataSource, NSTableViewDelegate

extension HomeViewController: NSTableViewDataSource, NSTableViewDelegate {

    func numberOfRows(in tableView: NSTableView) -> Int {

        return self.results.count
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        let item = self.results[row]

        switch item {
        case .header(let title):
            let headerView = HeaderView(title: title,
                                        theme: self.theme)

            return headerView

        case .mergeRequest(let mergeRequest):
            let mergeRequestView = MergeRequestView(theme: self.theme)
            mergeRequestView.configure(mergeRequest: mergeRequest)

            return mergeRequestView

        case .todo(let todo):
            let todoView = TodoView(theme: self.theme)
            todoView.delegate = self
            todoView.configure(todo: todo)

            return todoView

        case .text(let text):
            let textView = TextView(title: text)

            return textView
        }
    }
}

//MARK:- TableViewDelegate

extension HomeViewController: TableViewDelegate {

    func height(tableView: NSTableView, height: CGFloat) {

        guard height != 0.0,
            height != self.scrollViewTableViewHeightConstraint.constant else { return }

        print("New height: ", height, " Old Height: ", self.scrollViewTableViewHeightConstraint.constant)

        let newHeight = height > Constants.scrollViewTableViewHeightConstant ? Constants.scrollViewTableViewHeightConstant : height

        NSAnimationContext.runAnimationGroup({context in
            context.duration = 0.1
            context.allowsImplicitAnimation = true

            self.scrollViewTableViewHeightConstraint.animator().constant = newHeight

            self.view.layoutSubtreeIfNeeded()

        }, completionHandler:nil)
    }
}

//MARK:- HomeDataSourceDelegate

extension HomeViewController: HomeDataSourceDelegate {

    func homeDataSource(_ dataSource: HomeDataSource, results: [HomeResult], error: Error?) {

        self.progressIndicator.stopAnimation(nil)
        self.progressIndicator.isHidden = true
        self.updateProgressIndicator.stopAnimation(nil)
        self.updateProgressIndicator.isHidden = true
        self.updateButton.isHidden = false

        if error == nil {

            self.notification.presentNotificationsIfNeeded(oldTodos: HomeResultsHelper.todos(homeResults: self.results),
                                                           newTodos: HomeResultsHelper.todos(homeResults: results))
        }

        self.results = results
        self.tableView.reloadData()
    }

    func homeDataSource(_ dataSource: HomeDataSource,
                        todoToDone: Todo,
                        doneTodo: Todo?,
                        error: Error?) {

        var todoIndex: Int = 0

        todoIndex: for result in self.results {

            switch result {
            case .todo(let todo):
                if todo == todoToDone {

                    break todoIndex

                } else {

                    todoIndex += 1
                }

            case .header(_),
                 .mergeRequest(_),
                 .text(_):
                todoIndex += 1
            }
        }

        guard todoIndex != 0,
            doneTodo != nil,
            error == nil else {

            self.tableView.reloadData(forRowIndexes: IndexSet(integer: todoIndex),
                                      columnIndexes: IndexSet(integer: 0))

            return
        }

        self.results.remove(at: todoIndex)

        let numberOfTodos = self.results.filter {

            switch $0 {

            case .header(_),
                 .mergeRequest(_),
                 .text(_):

                return false

            case .todo(_):

                return true
            }
        }.count

        if numberOfTodos == 0 {

            self.results.insert(.text("Nothing to show 😮. keep up the good work 💻"), at: 1)

            self.tableView.beginUpdates()
            self.tableView.removeRows(at: IndexSet(integer: todoIndex), withAnimation: .effectFade)
            self.tableView.insertRows(at: IndexSet(integer: 1), withAnimation: .effectFade)
            self.tableView.endUpdates()

        } else {

            self.tableView.removeRows(at: IndexSet(integer: todoIndex), withAnimation: .effectFade)
        }
    }
}

//MARK:- TodoViewDelegate

extension HomeViewController: TodoViewDelegate {

    func doneButtonWasPressed(for todoView: TodoView) {

        let index = self.tableView.row(for: todoView)
        let item = self.results[index]

        switch item {
        case .todo(let todo):
            self.homeDataSource.doneTodo(todo: todo, gitlabAPI: self.gitLabAPI)

        default:
            return
        }
    }
}

//MARK:- Actions

extension HomeViewController {

    @objc func tableViewItemWasTapped() {

        guard self.results.count > 0 else { return }

        let item = self.results[self.tableView.clickedRow]

        switch item {

        case .todo(let todo):
            guard let url = URL(string: todo.targetUrl) else { return }

            NSWorkspace.shared.open(url)

        case .mergeRequest(let mergeRequest):
            guard let url = URL(string: mergeRequest.webUrl) else { return }

            NSWorkspace.shared.open(url)

        default:
            return
        }
    }

    @objc func updateButtonWasTapped() {

        self.updateButton.isHidden = true
        self.updateProgressIndicator.startAnimation(nil)
        self.updateProgressIndicator.isHidden = false

        self.homeDataSource.requestAll()
    }
}
