//
//  Core.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 21/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

class Core {

    let accountManager: AccountProtocol
    let factoryViewController: FactoryViewController
    let gitLabAPI: GitLabAPI
    let notificationManager: NotificationProtocol
    let router: Router
    let statusMenu: StatusMenuHandler
    let theme: ThemeProtocol

    init() {

        let factoryViewController = FactoryViewController()
        let router = Router(factoryViewController: factoryViewController)

        self.accountManager = AccountManager()
        self.factoryViewController = factoryViewController
        self.gitLabAPI =  GitLabAPI(account: self.accountManager, network: Network())
        self.notificationManager = NotificationManager(popoverProtocol: router)
        self.router = router
        self.statusMenu = StatusMenuHandler(popover: self.router)
        self.theme = ThemeManager()

        self.factoryViewController.core = self
    }
}
