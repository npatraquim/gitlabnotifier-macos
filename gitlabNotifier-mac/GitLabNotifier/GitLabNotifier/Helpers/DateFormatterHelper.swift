//
//  DateFormatterHelper.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 09/04/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

struct DateFormatterHelper {

    private static let dateFormatter: DateFormatter = {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()

    static func yyyyMMDDHourDate(date: Date) -> String {

        return dateFormatter.string(from: date)
    }
}
