//
//  HomeResultsHelper.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 18/05/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

struct HomeResultsHelper {

    static func todos(homeResults: [HomeResult]) -> [Todo] {

        var todos: [Todo] = []

        homeResults.forEach { element in

            switch element {
            case .todo(let todo):
                todos.append(todo)

            default:
                break
            }
        }

        return todos
    }
}
