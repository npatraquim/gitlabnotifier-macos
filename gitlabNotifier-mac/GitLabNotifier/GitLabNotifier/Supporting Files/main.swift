//
//  main.swift
//  WeatherBar
//
//  Created by Nuno Patraquim on 05/02/2019.
//  Copyright © 2019 none. All rights reserved.
//

import AppKit

let app: NSApplication = GNApplication.shared
let appDelegate = AppDelegate()  // Instantiates the class the @NSApplicationMain was attached to
app.delegate = appDelegate
_ = NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
